package net.rgielen.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBootTutor.
 *
 * @author Rene Gielen
 */
@SpringBootApplication
public class SpringBootTutor {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTutor.class, args);
    }

}
